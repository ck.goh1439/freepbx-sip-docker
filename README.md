# FreePBX on Docker

FreePBX container image for running a complete Asterisk server, which using Asterisk PBX version 16 based on Debian Jessie

With this container you can create a telephony system and integration to external VOIP providers with features such as call recording and interactive voice response configure jobs.

## Image includes

* Asterisk + FreePBX
* Modules: IVR, Time Conditions, Backup, Recording
* Automatic backup script


## Sample of docker-compose.yml

```yml
version: '3.3'
services:
  freepbx:
    image: dockerckgoh1439/build-freepbx:latest
    ports:
      - 8080:80
      - 5060:5060/udp
      - 5160:5160/udp
      - 3306:3306
      - 18000-18100:18000-18100/udp
    restart: always
    environment:
      - ADMIN_PASSWORD=admin123
    volumes:
      - backup:/backup
      - recordings:/var/spool/asterisk/monitor

volumes:
  backup:
  recordings:
```

## Usage

Fork the gitlab repository

    git clone https://gitlab.com/ck.goh1439/freepbx-sip-docker.git
    cd freepbx-sip-docker
    docker build -t build-freepbx:latest .

* Run ```docker-compose up -d```
* Run ```docker exec -it 'container-id' bash```
* Run ```a2enmod rewrite```
* Run ```service apache2 restart```

## Open admin panel

http://localhost:8080/admin/


## ENVs

* **/backup** - Backups are store inside a directory with freepbx version. Backup restore between different versions is not supported by Freepbx.
* **/var/spool/asterisk/monitor** - call recording storage location
* **/etc/asterisk/keys** - Let's Encrypt and self signed certificates pub/private keys generated in pbxadmin
